import asyncio

import aiodogstatsd


async def main():
    async with aiodogstatsd.Client() as client:
        client.increment("users.online")

asyncio.run(main())

